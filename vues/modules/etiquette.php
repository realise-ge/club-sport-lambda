<div class="card etiquetteCours border border-dark m-1" style="background: <?php echo $seance->getCouleur(); ?>;">
    <a href="/vues/cours.php?id=<?php echo $seance->getId(); ?>">
        <div class="card-body  text-dark">
            <h4 class="card-title"><?php echo $seance->getTitre(); ?></h4>
            <p class="card-text">
            <?php echo date("G\hi", strtotime($seance->getHeureDebut())).", ".$seance->getDuree()." min"; ?>
            </p>
        </div>
    </a>
</div>