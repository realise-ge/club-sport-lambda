<?php
// Ce fichier sert à processer les données du formulaire de login

// On va utiliser la session pour passer des messages d'une page à l'autre
// Pour cela il faut démarrer la session au début des pages concernées
session_start();

require_once(__DIR__ ."/../models/Database.php");
$database = new Database();

// On récupère les données du formulaire
$email = isset($_POST["email"]) ? $_POST["email"] : null;
$password = isset($_POST["password"]) ? $_POST["password"] : null;

// Vérifications
if($email == null || $password == null){
    $_SESSION["error"] = "L'email et le mot de passe sont obligatoires";
    header("location: ../vues/login.php");
    exit();
}

// On récupère le user en base de données
$user = $database->getUserByEmail($email);

// On vérifie que le user a été trouvé
if(!$user){
    $_SESSION["error"] = "L'email est incorrect, vous n'avez pas été trouvé";
    header("location: ../vues/login.php");
    exit();
}

// On vérifie que le user est actif
if($user->isActif() == 0){
    $_SESSION["error"] = "Votre compte n'a pas encore été validé, consultez vos emails";
    header("location: ../vues/login.php");
    exit();
}

// On vérifie le mot de passe
if(!password_verify($password, $user->getPassword())){
    $_SESSION["error"] = "Le mot de passe est incorrect";
    header("location: ../vues/login.php");
    exit();
}

// Tous les tests ont réussi donc on peut logguer le user
// On ajoute son id et l'objet user dans la session
$_SESSION["id"] = $user->getId();
$_SESSION["user"] = serialize($user);

// On redirige vers la page planning
$_SESSION["info"] = "Vous êtes bien connecté";
header("location: ../vues/planning.php");