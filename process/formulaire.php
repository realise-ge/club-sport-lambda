<?php
// Ce fichier sert à processer les données du formulaire

// On va utiliser la session pour passer des messages d'une page à l'autre
// Pour cela il faut démarrer la session au début des pages concernées
session_start();

require_once(__DIR__ ."/../models/Database.php");
$database = new Database();

// Déclaration de constantes pour les 3 cas
const CREATION = 1;
const MODIFICATION = 2;
const DUPLICATION = 3;

// Récupération des données du formulaire
$titre = isset($_POST["titre"]) ? $_POST["titre"] : "";
$description = isset($_POST["description"]) ? $_POST["description"] : "";
$heureDebut = isset($_POST["heureDebut"]) ? $_POST["heureDebut"] : "00:00";
$date = isset($_POST["date"]) ? $_POST["date"] : date("Y-m-d");
$duree = isset($_POST["duree"]) ? $_POST["duree"] : 50;
$nbParticipants = isset($_POST["nbParticipants"]) ? $_POST["nbParticipants"] : 10;
$couleur = isset($_POST["couleur"]) ? $_POST["couleur"] : "#ffffff";

$type = isset($_POST["type"]) ? $_POST["type"] : CREATION;
$id = isset($_POST["id"]) ? $_POST["id"] : null;

// Créons une séance avec les données reçues
$seance = Seance::createSeance($titre, $description, $heureDebut,
                                $date, $duree, $nbParticipants, $couleur);

$error = null;
$succes = null;

// On va créer ou modifier la séance en fonction du cas
switch($type){
    case CREATION :
    case DUPLICATION :
        // Dans ces 2 cas on crée une nouvelle séance
        $id = $database->createSeance($seance);
        if($id){
            $succes = "La séance a été créée avec succès";
        }else{
            $error = "La création de la séance a rencontré une erreur";
        }
        break;
    case MODIFICATION :
        $seance->setId($id);
        if($database->updateSeance($seance)){
            $succes = "La séance a été modifiée avec succès";
        }else{
            $error = "La modification de la séance a échoué";
        }
    default :
        // on ne fait rien
}

if($error == null){
    // On est dans le cas où l'opération s'est bien passée
    // On ajoute le message de succès dans la session
    $_SESSION["info"] = $succes;
    // On redirige vers la page de la séance
    header("Location: ../vues/cours.php?id=".$id);
}else{
    // Une erreur est survenue
    // On ajoute le message dans la session
    $_SESSION["error"] = $error;
    // On re-dirige vers le formulaire
    header("location: ../vues/formulaire.php?id".$id."&type=".$type);
}


?>