# Installation du projet

télécharger les containers docker sur https://gitlab.com/realise-ge/containerized-lamp 

Dans le dossier www clonez le projet actuel.

# Lancement des tests unitaires

Installez les dépendances de PHPUnit avec la commande : composer install.

Lancez les tests depuis le dossier www avec la commande : vendor/bin/phpunit test/*