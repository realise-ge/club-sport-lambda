<?php
include_once('Seance.php');
include_once('User.php');

/**
 * Classe de connexion à la base de donnée
 */

class Database{

    // Constantes de connexion
    const DB_HOST = "mariadb";
    const DB_PORT = "3306";
    const DB_NAME = "clublambda";
    const DB_USER = "adminClub";
    const DB_PASSWORD = "@!Mr8Qr36p";

    // Attribut de la classe
    protected $connexion;

    // Constructeur pour initier la connexion
    public function __construct(){
        try {
            $dsn = "mysql:host=".self::DB_HOST.";port=".self::DB_PORT.";dbname=".self::DB_NAME.";charset=UTF8";
            $this->connexion = new PDO($dsn, self::DB_USER, self::DB_PASSWORD);
        } catch (PDOException $e) {
            var_dump( 'Connexion échouée : ' . $e->getMessage());
        }
    } 

     /**
     * Fonction pour créer une nouvelle séance en base de donées
     * 
     * @param{Seance} seance : la séance à sauvegarder
     * 
     * @return{integer, boolean} l'id si la séance a été créée ou false sinon
     */
    public function createSeance(Seance $seance){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO seances(titre, description, heureDebut, date, duree, nbParticipantsMax, couleur) 
             VALUES (:titre, :description, :heureDebut, :date, :duree, :nbParticipantsMax, :couleur)"
        );
        // J'exécute ma requête en passant les valeur de l'objet Seance en valeur
        $pdoStatement->execute([
            "titre"             => $seance->getTitre(),
            "description"       => $seance->getDescription(),
            "heureDebut"        => $seance->getHeureDebut(),
            "date"              => $seance->getDate(),
            "duree"             => $seance->getDuree(),
            "nbParticipantsMax" => $seance->getNbParticipantsMax(),
            "couleur"           => $seance->getCouleur()
        ]);
        // Je récupère l'id créé si l'exécution s'est bien passée (code 0000 de MySQL)
        if($pdoStatement->errorCode() == 0){
            $id = $this->connexion->lastInsertId();
            return $id;
        }else{
            return false;
        }   
    }


    /**
     * Cette fonction cherche la seance dont l'id est passé en paramètre
     * et la retourne
     * 
     * @param{integer} id : l'id de la séance recherchée
     * 
     * @return{Seance|boolean} : un objet Seance si la séance a été trouvée, false sinon
     */
    public function getSeanceById($id){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM seances WHERE id = :id"
        );
        // J'exécute la requête en lui passant l'id
        $pdoStatement->execute(
            ["id" => $id]
        );
        // Je récupère le résultat
        $seance = $pdoStatement->fetchObject("Seance");
        return $seance;
    }

    /**
     * Fonction retourne toutes les séances de la semaine
     * 
     * @param{integer} week : le numéro de la semaine recherchée
     * 
     * @return{array} : un tableau de Seance s'il y a des séances programmées pour cette semaine
     */
    public function getSeanceByWeek($week){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM `seances` WHERE WEEKOFYEAR(date) = :week
             ORDER BY date, heureDebut"
        );
        // J'exécute la requête en lui passant le numéro de la semaine
        $pdoStatement->execute(
            ["week" => $week]
        );
        // Je récupère les résultats
        $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS,"Seance");
        return $seances;
    }

    /**
     * Fonction pour mettre à jour une séance en base de donées
     * 
     * @param{Seance} seance : la séance à mettre à jour
     * 
     * @return{boolean} true si la séance est mise à jour ou false sinon
     */
    public function updateSeance(Seance $seance){
        // Je prépare ma requête
        $pdoStatement = $this->connexion->prepare(
            "UPDATE seances
            SET titre = :titre, description = :description, heureDebut = :heureDebut, 
                date = :date, duree = :duree, nbParticipantsMax = :nbParticipantsMax, couleur = :couleur
            WHERE id = :id"
        );
        // J'exécute ma requête en passant les valeur de l'objet Seance en valeur
        $pdoStatement->execute([
            "titre"             => $seance->getTitre(),
            "description"       => $seance->getDescription(),
            "heureDebut"        => $seance->getHeureDebut(),
            "date"              => $seance->getDate(),
            "duree"             => $seance->getDuree(),
            "nbParticipantsMax" => $seance->getNbParticipantsMax(),
            "couleur"           => $seance->getCouleur(),
            "id"                => $seance->getId()
        ]);
        // Retourne true créé si l'exécution s'est bien passée (code 0000 de MySQL)
        if($pdoStatement->errorCode() == 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Fonction pour supprimer une séance en base de donées
     * 
     * @param{integer} id : l'id de la séance à supprimer
     * 
     * @return{boolean} true si la séance est supprimée ou false sinon
     */
    public function deleteSeance($id){
        // Je prépare la requete pour supprimer tous les inscrits à la séance
        $pdoStatement = $this->connexion->prepare(
            "Delete FROM inscrits WHERE id_seance = :seance"
        );
        // J'exécute ma requête
        $pdoStatement->execute(
            ["seance" => $id]
        );
        // Si ca ne s'est pas bien passé ce n'est pas la peine de continuer
        if($pdoStatement->errorCode() != 0){
            return false;
        }
        // Si les inscrits sont supprimés, je prépare la rwquete pour supprimer la séance
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM seances WHERE id = :seance"
        );
        // J'exécute ma requête
        $pdoStatement->execute(
            ["seance" => $id]
        );
        // Retourne true créé si l'exécution s'est bien passée (code 0000 de MySQL)
        if($pdoStatement->errorCode() == 0){
            return true;
        }else{
            return false;
        }
    }

    public function insertParticipant($idSeance, $idUser){
        // Je prépare la requete d'insertion
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO inscrits (id_user, id_seance) VALUES (:id_user, :id_seance)"
        );
        // J'exécute ma requête
        $pdoStatement->execute(
            ["id_user" => $idUser,
            "id_seance" => $idSeance]
        );
        // Retourne true créé si l'exécution s'est bien passée (code 0000 de MySQL)
        if($pdoStatement->errorCode() == 0){
            return true;
        }else{
            return false;
        }
    }

    public function deleteParticipant($idSeance, $idUser){
        // Je prépare la requete d'insertion
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM inscrits WHERE id_user = :id_user AND id_seance = :id_seance"
        );
        // J'exécute ma requête
        $pdoStatement->execute(
            ["id_user" => $idUser,
            "id_seance" => $idSeance]
        );
        // Retourne true créé si l'exécution s'est bien passée (code 0000 de MySQL)
        if($pdoStatement->errorCode() == 0){
            return true;
        }else{
            return false;
        }
    }

    public function deleteAllSeance(){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM seances;"
        );
        $pdoStatement->execute();
    }

    /*
     **************************************************
     *  Fonctions pour les users 
     * ************************************************
     */

     /**
     * Fonction pour créer un nouveau user en base de donées
     * 
     * @param{User} user : le user à sauvegarder
     * 
     * @return{integer, boolean} l'id si le user a été créé ou false sinon
     */
    public function createUser(User $user){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO users(nom, email, password, isAdmin, isActif, token) 
             VALUES (:nom, :email, :password, :isAdmin, :isActif, :token)"
        );
        // J'exécute ma requête en passant les valeur de l'objet User en valeur
        $pdoStatement->execute([
            "nom"         => $user->getNom(),
            "email"       => $user->getEmail(),
            "password"    => $user->getPassword(),
            "isAdmin"     => $user->isAdmin(),
            "isActif"     => $user->isActif(),
            "token"       => $user->getToken()
        ]);
        // Je récupère l'id créé si l'exécution s'est bien passée (code 0000 de MySQL)
        if($pdoStatement->errorCode() == 0){
            $id = $this->connexion->lastInsertId();
            return $id;
        }else{
            return false;
        }   
    }

    /**
     * Cette fonction cherche le user dont l'id est passé en paramètre
     * et le retourne
     * 
     * @param{integer} id : l'id du user recherché
     * 
     * @return{User|boolean} : un objet User si le user a été trouvé, false sinon
     */
    public function getUserById($id){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM users WHERE id = :id"
        );
        // J'exécute la requête en lui passant l'id
        $pdoStatement->execute(
            ["id" => $id]
        );
        // Je récupère le résultat
        $user = $pdoStatement->fetchObject("User");
        return $user;
    }

    /**
     * Active le user dont l'id est passé en paramètre
     * 
     * @param{integer} id : l'id du user à activer
     * 
     * @return{boolean} true si l'actvation s'est bien passée, false sinon
     */
    public function activateUser($id){
         // Je prépare ma requête
         $pdoStatement = $this->connexion->prepare(
            "UPDATE users
             SET isActif = 1
             WHERE id = :id"
        );
        // J'exécute ma requête en passant l'id' en valeur
        $pdoStatement->execute([
            "id" => $id
        ]);
        // Retourne true créé si l'exécution s'est bien passée (code 0000 de MySQL)
        if($pdoStatement->errorCode() == 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Vérifie si un email existe déjà dans la table users
     * 
     * @param{string} email : un email utilisé pour s'inscrire
     * 
     * @return{boolean} : true si l'email existe déjà, false sinon
     */
    public function isEmailExists($email){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT COUNT(*) FROM users WHERE email = :email"
        );
        // J'exécute la requête en lui passant l'email en valeur
        $pdoStatement->execute(
            ["email" => $email]
        );
        // Je récupère le résultat
        $nbUser = $pdoStatement->fetchColumn();
        // Si l'email n'a pas été trouvé retourner false
        if($nbUser == 0){
            return false;
        }else{
            // L'email a été trouvé
            return true;
        }
    }

    /**
     * Cette fonction cherche le user dont l'email est passé en paramètre
     * et le retourne
     * 
     * @param{string} email : l'email du user recherché
     * 
     * @return{User|boolean} : un objet User si le user a été trouvé, false sinon
     */
    public function getUserByEmail($email){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM users WHERE email = :email"
        );
        // J'exécute la requête en lui passant l'id
        $pdoStatement->execute(
            ["email" => $email]
        );
        // Je récupère le résultat
        $user = $pdoStatement->fetchObject("User");
        return $user;
    }

    /**
     *  Fonction qui permet de retrouver toutes les séances auxquelles est inscrit le user
     * 
     * @param{integer} id : l'id du user concerné
     * 
     * @return{array} un tableau contenant toutes les séances
     */
    public function getSeanceByUserId($idUser){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT s.* FROM seances s INNER JOIN inscrits i ON s.id = i.id_seance 
             WHERE i.id_user = :id_user ORDER BY s.date DESC, s.heureDebut DESC"
        );
        // J'exécute la requête en lui passant l'id du user
        $pdoStatement->execute(
            ["id_user" => $idUser]
        );
        // Je récupère les résultats
        $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS,"Seance");
        return $seances;
    }

    /**
     * Fonction qui nous permet de savoir si un utilisateur est inscrit à une séance
     * 
     * @param{integer} idUser : l'id de l'utilisateur
     * @param{integer} idSeance : l'id de la séance
     * 
     * @return{boolean} true si l'utilisateur est inscrit, false sinon
     */
    public function isInscrit($idUser, $idSeance){
        // Je prépare la requete d'insertion
        $pdoStatement = $this->connexion->prepare(
            "SELECT COUNT(*) FROM inscrits WHERE id_user = :id_user AND id_seance = :id_seance"
        );
        // J'exécute ma requête
        $pdoStatement->execute(
            ["id_user" => $idUser,
            "id_seance" => $idSeance]
        );
        // Je récupère le résultat
        $inscrit = $pdoStatement->fetchColumn();
        // Si aucune inscription n'a été trouvée retourner false
        if($inscrit == 0){
            return false;
        }else{
            // Une inscription a été trouvée
            return true;
        }
    }

    /**
     * Fonction qui retourne le nombre d'inscrits à une séance
     * 
     * @param{integer} idSeance : l'id de la séance
     * 
     * @return{integer} le nombre d'inscrits
     */
    public function nombreInscrits($idSeance){
        // Je prépare la requete d'insertion
        $pdoStatement = $this->connexion->prepare(
            "SELECT COUNT(*) FROM inscrits WHERE id_seance = :id_seance"
        );
        // J'exécute ma requête
        $pdoStatement->execute(
            ["id_seance" => $idSeance]
        );
        // Je récupère le résultat
        $nbInscrits = $pdoStatement->fetchColumn();
        // Retourne le nombre d'inscrit à cette séance
        return $nbInscrits;
    }


    public function deleteAllUser(){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM users;"
        );
        $pdoStatement->execute();
    }

    public function deleteAllInscrit(){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM inscrits;"
        );
        $pdoStatement->execute();
    }


}

?>
